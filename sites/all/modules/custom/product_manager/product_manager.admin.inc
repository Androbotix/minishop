<?php

function product_manager_config_form($form, $form_state) {

  $form['node_type'] = array(
    '#type' => 'select',
    '#title' => t('Node type to apply'),
    '#options' => product_manager_get_all_node_types(),
    '#default_value' => variable_get('product_manager_content_type', 0),
    '#prefix' => '<div id="dropdown-states">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'event'=>'change',
      'callback' =>'_node_type_callback',
      'wrapper' => 'dropdown-states',
    ),
  );

  $form['image_fields'] = array(
    '#type' => 'select',
    '#title' => t('Product image field'),
    // '#description' => 'Select product image field',
    '#options' => product_manager_get_all_image_fields(),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;
}

function product_manager_get_all_node_types() {
  $node_types = array();

  $nodes = node_type_get_types();

  if(empty($nodes)) {
    return array('No node type found!');
  }

  foreach($nodes as $node) {
    $type = $node->type;
    $node_types[$type] = $node->name;
  }

  return $node_types;
}

function product_manager_get_all_image_fields() {
  $results = array();
  $content_type = variable_get('product_manager_content_type');
  $fields = field_info_instances('node', $content_type);

  foreach($fields as $field_name => $val) {
    $field_info = field_info_field($field_name);
    if($field_info['type'] == 'image') {
      $results[$field_name] = $field_name;
    }
  }

  if(empty($results)) {
    $results[] = 'No image field found';
  }

  return $results;
}

function _node_type_callback($form, &$form_state) {
  variable_set('product_manager_content_type', $form_state['input']['node_type']);

  ctools_include('ajax');
  ctools_add_js('ajax-responder');
  $commands[] = ctools_ajax_command_redirect('admin/config/product_manager/config');
  print ajax_render($commands);
  exit;
}

function product_manager_config_form_submit($form, $form_state) {
  variable_set('product_manager_image_fields', $form_state['values']['image_fields']);
}
