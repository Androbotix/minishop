<?php

function wishlist_add_to_list($nid) {
  global $base_url;
  global $user;
  $node = node_load($nid);

  $results = db_select('wishlist', 'w')
      ->fields('w')
      ->condition('uid', $user->uid)
      ->condition('pid', $nid)
      ->execute()
      ->fetchAll();

  if(empty($results)) {
    db_insert('wishlist')
     ->fields(array(
         'pid' => $nid,
         'uid' => $user->uid,
         'created' => time(),
         'weight' => 10,
       ))
     ->execute();
  }
  else {
    dpm('This product has already added to your wishlist!');
  }
  drupal_goto($base_url . '/?q=list');
}
