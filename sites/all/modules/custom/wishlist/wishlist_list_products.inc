<?php

function wishlist_list_products() {
  $page = array();

  $table_header = array(
    'id' => t('Id'),
    'name' => t('Name'),
    'description' => t('Description'),
    'price' => t('Price'),
    'images' => t('Product images'),
    'wishlist' => '',
  );

  $table_data = wishlist_get_table_data();

  $page = theme('table', array('header' => $table_header, 'rows' => wishlist_get_table_data()));

  // //DB select

  // $result = db_select('wishlist', 'w')
  //   ->fields('w')
  //   //->condition('id', 2)
  //   ->orderBy('created', 'DESC')
  //   ->execute()
  //   ->fetchAll();

  //   dpm($result);

  // // DB update

  // db_update('wishlist')
  //   ->fields(array(
  //     'uid' => 11,
  //   ))
  //   ->condition('id', 2)
  //   ->execute();

  // //DB delete

  // db_delete('wishlist')
  //   ->condition('id', 2)
  //   ->execute();

  return $page;
}

function wishlist_get_table_data() {
  global $base_url;
  $data = array();
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'product');
  $results = $query->execute();

  $image_field = variable_get('product_manager_image_fields');

  foreach($results['node'] as $res) {
    $images = '';
    $node = node_load($res->nid, $res->vid);
    if(isset($node->$image_field)) {
      $field = $node->$image_field;
      foreach($field['und'] as $image) {
        $image_uri =  image_style_url('thumbnail', $image['uri']);
        $images .= '<img src="' . $image_uri . '">';
      }
    }

    $data[$node->nid] = array(
      'id' => $node->uid,
      'name' => '<a href="?q=node/' . $node->nid . '">' . $node->title . '</a>',
      'description' => $node->field_description['und'][0]['value'],
      'price' => $node->field_price['und'][0]['value'],
      'images' => $images,
      'wishlist' => t('<a href="?q=list/add/' . $node->nid . '">Add to wishlist</a>'),
    );
  }
  return $data;
}

function wishlist_get_products_for_menu() {
  global $user;
  $products = array();

  $results = db_select('wishlist', 'w')
    ->fields('w')
    ->condition('uid', $user->uid)
    ->orderBy('weight', 'DESC')
    ->execute()
    ->fetchAll();

  if(!empty($results)) {
    foreach($results as $res) {
      $node = node_load($res->pid);
      $products[] = array(
        'title' => $node->title,
        'price' => $node->field_price['und'][0]['value'],
        'image' => image_style_url('thumbnail',  $node->field_images['und'][0]['uri']),
      );
    }
  }

  return theme('dropdown_menu', array('items' => $products));
}





