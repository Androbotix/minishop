<?php
/**
 * @file
 * product_manager_features.features.inc
 */

/**
 * Implements hook_node_info().
 */
function product_manager_features_node_info() {
  $items = array(
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => t('Product content type.'),
      'has_title' => '1',
      'title_label' => t('product'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
